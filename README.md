# WebPing

Program for ping sites using http (https) protocol.
Used to verify the stability of network infrastructure.

The result of the program can be displayed on the screen or in the log file.
Only connection errors are displayed, if there are no problems - no information is displayed.

To display information on the screen please run the programm with  /console command line parameter.
  Webping /console

The program settings are stored in the webping.exe.config configuration file.

    <add key="PingTimeout" value="10000"/>  
    <add key="Url" value="https://www.google.com/"/>
    <add key="FileName" value="webping.log"/>
    <add key="MaxFileSizeBytes" value="10485760"/>
    <add key="UseHeadRequest" value="false"/>



