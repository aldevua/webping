﻿using System;
using System.IO;
using System.Net;
using System.Text;

namespace Webping
{
    class PingHttp
    {
        #region Public members

        public const string STATUS_OK = "Ok";

        #endregion

        #region private members


        private const int REQUSET_TIMEOUT = 2000;


        private static PingHttp instance;
        private AppConfig _config;

        private string Url { get; set; }

        #endregion

        #region .ctor
        private PingHttp()
        {
            this._config = AppConfig.GetInstance();
        }
        #endregion

        #region Public methods

        public string Ping()
        {
            ServicePointManager.SecurityProtocol = ServicePointManager.SecurityProtocol
                                                   | SecurityProtocolType.Tls11
                                                   | SecurityProtocolType.Tls12;
            try
            {
                HttpWebRequest myWebReq = null;
                var sUrl = new Uri(this._config.Url);
                myWebReq = (HttpWebRequest) WebRequest.Create(sUrl);
                myWebReq.Timeout = REQUSET_TIMEOUT;
                myWebReq.AllowAutoRedirect = false; // find out if this site is up and don't follow a redirector
                if (this._config.UseHeadRequest)
                {
                    myWebReq.Method = "HEAD";
                }
                using (var myWebRes = (HttpWebResponse)myWebReq.GetResponse())
                {
                    return myWebRes.StatusCode == HttpStatusCode.OK 
                        ? STATUS_OK 
                        : myWebRes.StatusCode.ToString();
                }
            }
            catch (WebException wex)
            {
                return wex.Message;
            }
        }

        #endregion

        public static PingHttp GetInstance()
        {
            return instance ?? (instance = new PingHttp());
        }

        #region Private methods



        #endregion
    }
}
