﻿namespace Webping
{
    public class AppConfig
    {
        #region private members

        private const string PING_TIMEOUT_APP_KEY = "PingTimeout";
        private const int DEFAULT_PING_TIMEOUT = 10000;
        private const string FILE_NAME_APP_KEY = "FileName";
        private const string URL_APP_KEY = "Url";
        private const string MAX_FILE_SIZE_BYTES_APP_KEY = "MaxFileSizeBytes";
        private const int DEFAULT_MAX_FILE_SIZE_BYTES_APP_KEY = 4194304;
        private const string USE_HEAD_REQUEST_APP_KEY = "UseHeadRequest";

        private static AppConfig _instance;

        #endregion

        #region .ctor
        private AppConfig()
        { }
        #endregion

        #region Public properties

        public int PingTimeOut => this.GetIntValue(PING_TIMEOUT_APP_KEY, DEFAULT_PING_TIMEOUT);

        public string Url => this.GetStringValue(URL_APP_KEY, string.Empty);

        public string FileName => this.GetStringValue(FILE_NAME_APP_KEY, string.Empty);

        public int MaxFileSize => this.GetIntValue(MAX_FILE_SIZE_BYTES_APP_KEY, DEFAULT_MAX_FILE_SIZE_BYTES_APP_KEY);

        public bool UseHeadRequest => this.GetBoolValue(USE_HEAD_REQUEST_APP_KEY, false);

        #endregion

        public static AppConfig GetInstance()
        {
            return _instance ?? (_instance = new AppConfig());
        }

        #region Private methods

        private int GetIntValue(string name, int defaultValue)
        {
            return int.TryParse(System.Configuration.ConfigurationManager.AppSettings[name], out var t) 
                ? t 
                : defaultValue;
        }

        private bool GetBoolValue(string name, bool defaultValue)
        {
            return bool.TryParse(System.Configuration.ConfigurationManager.AppSettings[name], out var t)
                ? t
                : defaultValue;
        }

        private string GetStringValue(string name, string defaultValue)
        {
            var t = System.Configuration.ConfigurationManager.AppSettings[name];
            return !string.IsNullOrEmpty(t) 
                ? t 
                : defaultValue;
        }

        #endregion


    }
}
