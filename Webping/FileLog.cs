﻿using System;
using System.IO;
using System.Net;

namespace Webping
{
    class FileLog
    {
        #region private members

        private static FileLog _instance;
        private readonly AppConfig _config;

        private string FileName { get; }

        #endregion

        #region .ctor
        private FileLog()
        {
            this._config = AppConfig.GetInstance();
            this.FileName = this._config.FileName;
        }
        #endregion

        #region Public methods

        public void Log(string message)
        {
            try
            {
                var fi = new FileInfo(this.FileName);
                if(fi.Exists && fi.Length > _config.MaxFileSize)
                {
                    File.WriteAllText(this.FileName, string.Empty);
                }

                File.AppendAllText(this.FileName, message + Environment.NewLine);
            }
            catch (WebException wex)
            {
                Console.WriteLine(wex.Message);
            }
        }

        #endregion

        public static FileLog GetInstance()
        {
            return _instance ?? (_instance = new FileLog());
        }

        #region Private methods
        
        #endregion
    }
}
