﻿using System;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace Webping
{
    /// <summary>
    /// WebPing, 11/03/2019
    /// </summary>
    class Program
    {
        #region Constants

        private const string RUN_IN_CONSOLE_MODE_PARAMETER = "/console";
        private const string ERRORS_OUTPUT_TO_CONSOLE_MESSAGE = "errors output to console";
        private const string ERRORS_OUTPUT_TO_LOG_FILE_MESSAGE = "errors output to log file";

        #endregion

        static void Main(string[] args)
        {

            var appSetting = AppConfig.GetInstance();
            var pingModule = PingHttp.GetInstance();
            var fileLog = FileLog.GetInstance();

            var isConsoleMode = args.Any() && args[0].ToLower() == RUN_IN_CONSOLE_MODE_PARAMETER;

            Console.WriteLine("Press ESC (or [E]xit, [Q]uit) to stop");
            Console.Write($"Ping for {appSetting.Url} started. ");
            Console.Write(isConsoleMode 
                ? ERRORS_OUTPUT_TO_CONSOLE_MESSAGE
                : ERRORS_OUTPUT_TO_LOG_FILE_MESSAGE);
            Console.Write(Environment.NewLine);

            var key = default(ConsoleKeyInfo);
            do
            {
                while (!Console.KeyAvailable)
                {
                    // Do something
                    var response = pingModule.Ping();
                    if (response != PingHttp.STATUS_OK)
                    {
                        var result = $"{DateTime.Now}, response: {response}";
                        if (isConsoleMode)
                            Console.WriteLine(result);
                        else
                            fileLog.Log(result);
                    }
                    if (!isConsoleMode)
                    {
                        Console.SetCursorPosition(0, 2);
                        Console.Write(DateTime.Now.ToString(CultureInfo.InvariantCulture));
                    }
                    Task.Delay(appSetting.PingTimeOut).Wait();
                }
                key = Console.ReadKey(true);
            } while (key.Key != ConsoleKey.Escape && key.Key != ConsoleKey.Q && key.Key != ConsoleKey.E);

            Console.WriteLine($"\nPing has been stopped at {DateTime.Now.ToString(CultureInfo.InvariantCulture)}.");
        }
    }
}
